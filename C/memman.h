#ifndef JAY_CB_MEMMAN_H
#define JAY_CB_MEMMAN_H

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdint.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>


extern void *jay_cb_malloc(size_t size);
extern int jay_cb_freethemall(uint8_t n, ...);

#endif

