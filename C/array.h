#ifndef JAY_CB_ARRAY_H
#define JAY_CB_ARRAY_H

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>

#include "macro.h"
#include "memman.h"

enum jay_cb_array_type {
JAY_CB_ARRAY_TYPE_UNINIT,
JAY_CB_ARRAY_TYPE_U8 = 2,
JAY_CB_ARRAY_TYPE_I8,
JAY_CB_ARRAY_TYPE_U16 = 4,
JAY_CB_ARRAY_TYPE_I16,
JAY_CB_ARRAY_TYPE_U32 = 8,
JAY_CB_ARRAY_TYPE_I32,
JAY_CB_ARRAY_TYPE_U64 = 16,
JAY_CB_ARRAY_TYPE_I64,
JAY_CB_ARRAY_TYPE_F32 = 108,
JAY_CB_ARRAY_TYPE_F64 = 116,
};

struct jay_cb_array {
	void *data;
	uint64_t nelements;
	uint8_t ndim;
	uint64_t *dims;
	uint64_t *dimscoef;
	enum jay_cb_array_type type;
	
	double (*getdata) (struct jay_cb_array *, uint64_t[]);
	void (*setdata) (struct jay_cb_array *, uint64_t[], double);
	double (*getindex) (struct jay_cb_array *, uint64_t);
	void (*setindex) (struct jay_cb_array *, uint64_t, double);
	void (*setdim) (struct jay_cb_array *, uint8_t , uint64_t[]);
	void (*settype) (struct jay_cb_array *, enum jay_cb_array_type);
	void (*destroy) (struct jay_cb_array **);
};

#ifndef NAN
#define NAN 0./0.
#endif

extern struct jay_cb_array *jay_cb_array_init(uint8_t ndim, uint64_t dims[], enum jay_cb_array_type typecode);

#endif

