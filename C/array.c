#include "array.h"

#ifdef JAY_CB_ARRAY_NOLIB
int main(int argc, char *argv[])
{
	struct jay_cb_array *array = jay_cb_array_init(2, (uint64_t[]) {3462, 9386}, JAY_CB_ARRAY_TYPE_F32);
	int32_t index = 63542;
	double v = 3.982;
	
	array->setindex(array, index, v);
	JAY_CB_ECHOWITHTIME("set array[%d, %d] %f", index/9386, index%9386, v);
	JAY_CB_ECHOWITHTIME("get array[%d, %d] %f", index/9386, index%9386, array->getindex(array, index));
	
	array->destroy(&array);
	
	struct jay_cb_array *array2 = jay_cb_array_init(0, NULL, JAY_CB_ARRAY_TYPE_UNINIT);
	uint64_t pos[] = {1, 40, 60};
	v = 129;//cast to int8
	
	array2->settype(array2, JAY_CB_ARRAY_TYPE_I8);
	array2->setdim(array2, 3, (uint64_t[]){3, 60, 121});
	array2->setdata(array2, pos, v);
	JAY_CB_ECHOWITHTIME("set array[%d, %d, %d] %f", pos[0], pos[1], pos[2], v);
	JAY_CB_ECHOWITHTIME("get array[%d, %d, %d] %f", pos[0], pos[1], pos[2], array2->getdata(array2, pos));
	array2->destroy(&array2);
}
#endif

static uint64_t jay_cb_array_dim2index(struct jay_cb_array *array, uint64_t dim[])
{
	uint64_t index = 0;
	
	for (uint8_t idim = 0; idim < array->ndim; ++idim)
		index += array->dimscoef[idim] * dim[idim];
	
	return index;
}

static inline uint8_t jay_cb_array_isempty(struct jay_cb_array *array)
{
	return !(array->data || array->dims || array->dimscoef);
}

static double jay_cb_array_getindex(struct jay_cb_array *array, uint64_t index)
{
	if (jay_cb_array_isempty(array))
		return NAN;

	switch (array->type) {
	case (JAY_CB_ARRAY_TYPE_U8):  return (double) *(((uint8_t  *) array->data) + index);
	case (JAY_CB_ARRAY_TYPE_I8):  return (double) *(((int8_t   *) array->data) + index);
	case (JAY_CB_ARRAY_TYPE_U16): return (double) *(((uint16_t *) array->data) + index);
	case (JAY_CB_ARRAY_TYPE_I16): return (double) *(((int16_t  *) array->data) + index);
	case (JAY_CB_ARRAY_TYPE_U32): return (double) *(((uint32_t *) array->data) + index);
	case (JAY_CB_ARRAY_TYPE_I32): return (double) *(((int32_t  *) array->data) + index);
	case (JAY_CB_ARRAY_TYPE_U64): return (double) *(((uint64_t *) array->data) + index);
	case (JAY_CB_ARRAY_TYPE_I64): return (double) *(((int64_t  *) array->data) + index);
	case (JAY_CB_ARRAY_TYPE_F32): return (double) *(((float    *) array->data) + index);
	case (JAY_CB_ARRAY_TYPE_F64): return (double) *(((double   *) array->data) + index);
	default: return NAN;
	}
}

static double jay_cb_array_getdata(struct jay_cb_array *array, uint64_t dim[])
{
	return jay_cb_array_getindex(array, jay_cb_array_dim2index(array, dim));
}

static void jay_cb_array_setindex(struct jay_cb_array *array, uint64_t index, double value)
{
	if (jay_cb_array_isempty(array))
		return;

	switch (array->type) {
	case (JAY_CB_ARRAY_TYPE_U8):  *(((uint8_t  *) array->data) + index) = value; break;
	case (JAY_CB_ARRAY_TYPE_I8):  *(((int8_t   *) array->data) + index) = value; break;
	case (JAY_CB_ARRAY_TYPE_U16): *(((uint16_t *) array->data) + index) = value; break;
	case (JAY_CB_ARRAY_TYPE_I16): *(((int16_t  *) array->data) + index) = value; break;
	case (JAY_CB_ARRAY_TYPE_U32): *(((uint32_t *) array->data) + index) = value; break;
	case (JAY_CB_ARRAY_TYPE_I32): *(((int32_t  *) array->data) + index) = value; break;
	case (JAY_CB_ARRAY_TYPE_U64): *(((uint64_t *) array->data) + index) = value; break;
	case (JAY_CB_ARRAY_TYPE_I64): *(((int64_t  *) array->data) + index) = value; break;
	case (JAY_CB_ARRAY_TYPE_F32): *(((float    *) array->data) + index) = value; break;
	case (JAY_CB_ARRAY_TYPE_F64): *(((double   *) array->data) + index) = value; break;
	default: ;
	}
}

static void jay_cb_array_setdata(struct jay_cb_array *array, uint64_t dim[], double value)
{
	jay_cb_array_setindex(array, jay_cb_array_dim2index(array, dim), value);
}

static void jay_cb_array_destroy(struct jay_cb_array **array)
{
	jay_cb_freethemall(4, &(*array)->data, &(*array)->dims, &(*array)->dimscoef, array);
}

static void jay_cb_array_settype(struct jay_cb_array *array, enum jay_cb_array_type typecode)
{
	if (array->type != typecode) {
		array->type = typecode;
		
		if (typecode)
			array->data = reallocarray(array->data, array->nelements, (array->type%100)/2);
		else
			LUNA_FREE(array->data);
	}
}

static void jay_cb_array_setdim(struct jay_cb_array *array, uint8_t ndim, uint64_t dims[])
{
	if (!ndim)
		return;
	
	array->ndim = ndim;
	array->dimscoef = reallocarray(array->dimscoef, ndim, sizeof(uint64_t));
	array->dimscoef[--ndim] = 1;
	while (ndim-->0) {
		array->dimscoef[ndim] = array->dimscoef[ndim+1] * dims[ndim+1];
	}
	array->nelements = array->dimscoef[0] * dims[0];
	array->dims = reallocarray(array->dims, ndim, sizeof(uint64_t));
	memcpy(array->dims, dims, sizeof(uint64_t[ndim]));
	
	if (array->type)
		array->data = reallocarray(array->data, array->nelements, (array->type%100)/2);
}

static struct jay_cb_array *jay_cb_array_init_null()
{
	struct jay_cb_array *ret = jay_cb_malloc(sizeof(struct jay_cb_array));
	
	ret->ndim = 0;
	ret->nelements = 0;
	
	ret->data = NULL;
	ret->dims = NULL;
	ret->dimscoef = NULL;
	
	ret->type = JAY_CB_ARRAY_TYPE_UNINIT;
	
	ret->getdata  = jay_cb_array_getdata;
	ret->setdata  = jay_cb_array_setdata;
	ret->getindex = jay_cb_array_getindex;
	ret->setindex = jay_cb_array_setindex;
	
	ret->setdim  = jay_cb_array_setdim;
	ret->settype = jay_cb_array_settype;
	ret->destroy = jay_cb_array_destroy;
	
	return ret;
}

struct jay_cb_array *jay_cb_array_init(uint8_t ndim, uint64_t dims[], enum jay_cb_array_type typecode)
{
	struct jay_cb_array *ret = jay_cb_array_init_null();
	
	jay_cb_array_settype(ret, typecode);
	jay_cb_array_setdim(ret, ndim, dims);
	
	return ret;
}

