#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "macro.h"
#include "memman.h"


void *jay_cb_malloc(size_t size)
{
	void *ptr = malloc(size);
	
	if (ptr) {
		return ptr;
	} else {
		JAY_CB_ERRMEM(ptr);
		exit(1);//TODO exit code macro
	}
}

int jay_cb_freethemall(uint8_t n, ...)
{
	va_list ap;
	void **p;
	
	va_start(ap, n);
	while (n-- > 0) {
		p = va_arg(ap, void **);
		JAY_CB_FREE(*p);
	}
	va_end(ap);
	
	return 0;
}

