#ifndef JAY_CB_MACRO_H
#define JAY_CB_MACRO_H

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

/*
 * Safer free macro fn
 */
#define JAY_CB_FREE(ptr) \
	do { \
		if (ptr) { \
			free(ptr); \
			ptr = NULL; \
		} \
	} while (0)


/*
 * Print messages with leading current time and an ending '\n'
 */
#define JAY_CB___ECHOWITHTIME(stream, ...) \
	do { \
		time_t jay_cb_global_time; \
		time(&jay_cb_global_time); \
		fprintf(stream, "[%15.15s] ", ctime(&jay_cb_global_time)+4); \
		fprintf(stream, __VA_ARGS__); \
		fprintf(stream, "\n"); \
	} while (0)
#define JAY_CB_ECHOWITHTIME(...) JAY_CB___ECHOWITHTIME(stdout, __VA_ARGS__)


/*
 * Error prompt
 */
#define JAY_CB_ERRECHOWITHTIME(...) JAY_CB___ECHOWITHTIME(stderr, __VA_ARGS__)
#define JAY_CB_ERRLOC               JAY_CB_ERRECHOWITHTIME("File: %s, Fn: %s, Ln: %d", __FILE__, __FUNCTION__, __LINE__)

#define JAY_CB_ERROPEN(fname) \
	do { \
		JAY_CB_ERRECHOWITHTIME("ERROR %d %s: %s", errno, strerror(errno), fname); \
		JAY_CB_ERRLOC; \
	} while (0)

#define JAY_CB_ERRFIO(fd) \
	do { \
		close(fd); \
		JAY_CB_ERRECHOWITHTIME("ERROR %d %s", errno, strerror(errno)); \
		JAY_CB_ERRLOC; \
	} while (0)

#define JAY_CB_ERRMEM(ptr) \
	do { \
		JAY_CB_FREE(ptr); \
		JAY_CB_ERRECHOWITHTIME("MEM PANIC %d %s", errno, strerror(errno)); \
		JAY_CB_ERRLOC; \
	} while (0)


#endif
