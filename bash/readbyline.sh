#!/usr/bin/env bash

fname=$1
foo(){ :;}

# Method using fd
# PRO: process flexible lines per time
# CON: NO empty line in file
NLINEPERTIME=1
line=""
exec 5< ${fname}

go_on=true
while ${go_on}; do
	for i in $(seq ${NLINEPERTIME}); do
		read line <&5
		if [ "x" = "x${line}" ]; then
			go_on=false
			break
		fi
		foo line & # C'mon, do something
	done
	wait
done

exec 5<&-


# Method 2
while IFS= read -r line; do
	foo line # C'mon, do something
done < ${fname}
